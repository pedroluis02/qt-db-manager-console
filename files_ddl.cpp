#include "files_ddl.h"


bool FileDDL_DB::create_FDDL(QString dir_file){

    if(QFile::exists(dir_file))
        return false;
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite);
    file.seek(0);
    Header_F reg_h;
    reg_h.reg_numbers = 0; reg_h.reg_emptys = 0;
    file.write((char*)&reg_h, sizeof(class Header_F));

    file.close();
    return true;
}

bool FileDDL_DB::create_FDDL_Empty(QString dir_file){
    if(QFile::exists(dir_file))
        return false;
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite);
    file.close();
    return true;
}

bool FileDDL_DB::write_FDDL(QString dir_file, Reg_Data reg_in){
    QFile file(dir_file);
    bool state = file.open(QIODevice::ReadWrite);

    Header_F reg_h;
    file.read((char*)&reg_h, sizeof(class Header_F));

    if(reg_h.reg_emptys == 0){
        reg_in.reg_number = reg_h.reg_numbers;
        file.seek(reg_h.reg_numbers*sizeof(class Reg_Data) + sizeof(class Header_F));
        file.write((char*)&reg_in, sizeof(class Reg_Data));
    }
    else{
        file.seek(sizeof(class Header_F));
        while(!file.atEnd()){
            Reg_Data reg_aux;
            file.read((char*)&reg_aux, sizeof(class Reg_Data));
            if(reg_aux.state == 0){
                file.seek(reg_aux.reg_number*sizeof(class Reg_Data) + sizeof(class Header_F));
                file.write((char*)&reg_in, sizeof(class Reg_Data));
                reg_h.reg_emptys--;
                break;
            }
        }
    }
    file.seek(0);
    reg_h.reg_numbers++;
    file.write((char*)&reg_h, sizeof(class Header_F));

    file.close();
    return state;
}

Reg_Data FileDDL_DB::read_FDDL(QString dir_file, int reg_number){
    QFile file(dir_file);
    Reg_Data reg_data;
    file.open(QIODevice::ReadOnly);
    file.seek(reg_number*sizeof(class Reg_Data) + sizeof(class Header_F));
    file.read((char*)&reg_data, sizeof(class Reg_Data));
    file.close();
    return reg_data;
}

bool FileDDL_DB::delete_FDDL(QString dir_file, int reg_number){
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite);
    Reg_Data reg_data;
    file.seek(reg_number*sizeof(class Reg_Data) + sizeof(class Header_F));
    file.read((char*)&reg_data, sizeof(class Reg_Data));

    reg_data.state = 0;
    file.seek(reg_number*sizeof(class Reg_Data) + sizeof(class Header_F));
    file.write((char*)&reg_data, sizeof(class Reg_Data));

    file.seek(0);
    Header_F reg_h;
    file.read((char*)&reg_h, sizeof(class Header_F));

    reg_h.reg_numbers--;
    reg_h.reg_emptys++;

    file.seek(0);
    file.write((char*)&reg_h, sizeof(class Header_F));
    file.close();
    return true;
}

QStringList FileDDL_DB::readAll_FDDL(QString dir_file){
    QFile file(dir_file);
    QStringList listDBs;
    file.open(QIODevice::ReadOnly);
    file.seek(sizeof(class Header_F));
    while(!file.atEnd()){
        Reg_Data reg_aux;
        file.read((char*)&reg_aux, sizeof(class Reg_Data));
        if(reg_aux.state == 1){
            listDBs.append(reg_aux.item_name);
        }
    }
    file.close();
    return listDBs;
}

int FileDDL_DB::search_FDDL(QString dir_file, QString item_s){
    QFile file(dir_file);
    int s = -1;
    file.open(QIODevice::ReadOnly);
    file.reset();
    file.seek(sizeof(class Header_F));
    while(!file.atEnd()){
        Reg_Data reg_aux;
        file.read((char*)&reg_aux, sizeof(class Reg_Data));
        if(reg_aux.state == 1){
            if(item_s == QString(reg_aux.item_name)){
                s = reg_aux.reg_number;
                break;
            }
        }
    }
    return s;
}

int FileDDL_DB::numRegs(QString dir_file){
    QFile file(dir_file);
    return file.size() / sizeof(class Reg_Data);
}


bool DirDDL_DB::removeDir_DB(QString dir_pro, QString db_name){
    QDir dir(dir_pro + "/" + db_name);
    foreach (QFileInfo f, dir.entryInfoList()) {
        if(f.fileName() == "." || f.fileName() == "..")
            continue;
        if(f.isFile()){
            QFile::remove(f.absoluteFilePath());
        }
        else{
            DirDDL_DB::removeDir_Table(dir_pro + "/" + db_name,
                                      f.fileName());
        }
    }
    dir.cd("..");
    dir.rmdir(db_name);
    return true;
}

bool DirDDL_DB::removeDir_Table(QString dir_db, QString table_name){
    QDir dir(dir_db + "/" + table_name);
    foreach (QFileInfo f, dir.entryInfoList()) {
        QFile::remove(f.absoluteFilePath());
        //qDebug() << f.fileName();
    }
    dir.cd("..");
    dir.rmdir(table_name);
    return true;
}

////////////////////////

bool FileStruct_Table::write_AllColums(QString dir_file, QList<Reg_Colum> regs){
    QFile file(dir_file);
    bool res = file.open(QIODevice::ReadWrite);
    int i = 0;
    while(i < regs.size()){
        file.seek(i*sizeof(class Reg_Colum));
        file.write((char*)&(regs.at(i)), sizeof(class Reg_Colum));
        i++;
    }
    file.close();
    return res;
}

QList<Reg_Colum> FileStruct_Table::read_AllColums(QString dir_file){
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite);
    QList <Reg_Colum> regs;
    while(!file.atEnd()){
        Reg_Colum reg_c;
        file.read((char*)&reg_c, sizeof(class Reg_Colum));
        regs << reg_c;
    }
    file.close();
    return regs;
}

////////////////////////////////////////////

long FileData_Table::write_dataRow(QString dir_file, QStringList news_params){
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite | QIODevice::Append);
    long s = file.size();
    QTextStream tx(&file); int i = 0;
    foreach (QString v, news_params) {
        if(i == (news_params.size() - 1))
            tx << v << " # ";
        else
            tx << v << " | ";
        i++;
    }
    file.close();
    return s;
}

QList <QString*> FileData_Table::readAll_DataT(QString dir_file, QList <long> indexs,
                                   QList<Reg_Colum> columns_out){
    QFile file(dir_file);
    QList <QString*> dataRows;
    QStringList aux_row;
    file.open(QIODevice::ReadOnly);
    QTextStream tx(&file); int n = 0, i = 0;
    foreach (long b, indexs) {
        tx.seek(b);
        QString aux, data = "";
        n = 0;
        while(true){
            tx >> aux;
            if(aux == "|"){
               aux_row.append(data);
               data = "";
               n = 0;
            }
            else if(aux == "#"){
                aux_row.append(data);
                break;
            }
            else{
                if(n == 0){
                   data = aux;
                }
                else{
                     data += " " + aux;
                }
                n++;
            }
        }

        QString *row = new QString[columns_out.size()];

        i = 0;
        foreach (Reg_Colum reg_c, columns_out) {
            row[i] = aux_row.value(reg_c.reg_number);
            i++;
        }

        dataRows.append(row);
        aux_row.clear();
    }
    file.close();
    return dataRows;
}


bool FileIndex_Table::write_RegIndex(QString dir_file, Reg_Index reg_i){
    QFile file(dir_file);
    file.open(QIODevice::ReadWrite);
    int num_regs = file.size() / sizeof(class Reg_Index);
    if(num_regs == 0){
        reg_i.left = -1;
        reg_i.reg_number = num_regs;
        reg_i.right = -1;

        file.seek(0);
        file.write((char*)&reg_i, sizeof(class Reg_Index));
    }
    else{
        file.seek((num_regs - 1)*sizeof(class Reg_Index));
        Reg_Index aux;
        file.read((char*)&aux, sizeof(class Reg_Index));

        aux.right = num_regs;
        file.seek((num_regs - 1)*sizeof(class Reg_Index));
        file.write((char*)&aux, sizeof(class Reg_Index));

        reg_i.left = (num_regs - 1);
        reg_i.reg_number = num_regs;
        reg_i.right = -1;

        file.seek(num_regs*sizeof(class Reg_Index));
        file.write((char*)&reg_i, sizeof(class Reg_Index));
    }
    file.close();
    return true;
}

QList <long> FileIndex_Table::readAllIndex(QString dir_file){
    QFile file(dir_file);
    file.open(QIODevice::ReadOnly);
    QList <long> indexs; int i = 0;
    long size_r = sizeof(class Reg_Index);
    while(!file.atEnd()){
        Reg_Index reg_i;
        file.seek(i*size_r);
        file.read((char*)&reg_i, size_r);
        if(reg_i.data_position != -1)
            indexs.append(reg_i.data_position);
        i++;
    }
    file.close();
    return indexs;
}

long FileIndex_Table::search_Primarykey(QString dir_file, QString key){
    QFile file(dir_file);
    file.open(QIODevice::ReadOnly);
    int i = 0;long s = -1;

    if(file.size() == 0)
        return s;

    while(i != -1){
        file.seek(i*sizeof(class Reg_Index));
        Reg_Index reg_i;
        file.read((char*)&reg_i, sizeof(class Reg_Index));
        //qDebug() << reg_i.primary_key << reg_i.data_position << reg_i.left << reg_i.right;
        if(reg_i.data_position != -1){
            if(reg_i.primary_key == key){
                s = reg_i.data_position;
                break;
            }
            else
                i = reg_i.right;
        }
    }
    file.close();
    return s;
}
