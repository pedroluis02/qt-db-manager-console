# DataBase Manager console
Database manager console UI using indexed files and fixed records.

#### Files Struct: Data
Name | Description
| --- | --- |
dataPL | Main data folder.
databases | names of databases created.
<DATABASES_DIRS\> | Databases directories.

#### Files Struct: DataBase
Name | Description
| --- | --- |
tables | Names of tables created. 
<DATABASE_DIR\> | Database directory.

### Files Struct: Table
Name | Description
| --- | --- |
struct | Struct of table when it was created.
index | Indexes of table columns. For example primary key.
<TABLE_NAME\> | data rows of table.
