#ifndef TEXTA_CONSOLE_H
#define TEXTA_CONSOLE_H

#include <QTextEdit>
#include <QKeyEvent>
#include <QTextBlock>
#include <QTextTable>
#include <QTextList>

#include <QScrollBar>
#include <QMessageBox>

#include <QList>

#include "gestor_ddl.h"

class TextA_Console : public QTextEdit
{
    Q_OBJECT
public:
    explicit TextA_Console(QWidget *parent = 0);
    void keyPressEvent(QKeyEvent *e);
    QStringList separateTokens(QString in);
    ~TextA_Console();
private:
    void set_in_Console_();
    void set_out_list(QString title, QStringList dbs);
    void set_out_table_describe(QList <Reg_Colum> regs);
    void set_out_table_select(QStringList columns_names,
                              QList <QString*> &dataRow);
    //messages
    void printError_in(QString token_stop);
    void printError_incomplete();
    void printMessage(QString message);
    //

    // analyze ---
    int isIdentifier(QString token_text);
    //int isCharacter(QString token_text);
    int isString(QString token_text);
    int isInteger(QString token_text);
    int isReal(QString token_text, bool &decimal);
    int isNumber(QString token_text, bool &decimal);
    //

    //sin
    void parsing_tokens(QStringList tks);
    void analyze_create(QStringList tks);
    void analyze_structTable(QStringList tks, QString new_table, int i);
    void analyze_describe(QStringList tks);
    void analyze_show(QStringList tks);
    void analyze_use(QStringList tks);
    void analyze_drop(QStringList tks);

    void analyze_insert(QStringList tks);
    void analyze_select(QStringList tks);
    //

    DDL_TablesPL ddl_table;

    int in_numbers;

    QList <QTextTable *> textTables;
    QStringList list_tokens;
    QStringList list_types;

    //tokens
};

#endif // TEXTA_CONSOLE_H
