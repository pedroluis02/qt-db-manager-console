#include <QApplication>
#include "mainwindow.h"

#include <QTextStream>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

 /*   QFile file(QDir::currentPath()+"/config/prueba");
    file.open(QIODevice::ReadWrite | QIODevice::Truncate);
    QTextStream tx(&file);

    tx << 12 << " " << '|' << " ";
    tx << "pedro luis" << " " << '|' << " ";
    tx << 'c' << " " << '|' << " ";
    tx << -123.023;
    tx << " " << "#" << " ";
    tx.seek(0);
    QString data = "", aux;
    int n = 0;
    while(!tx.atEnd()){
        tx >> aux;
        if(aux == "|"){
           qDebug() << data;
           data = "";
           n = 0;
        }
        else if(aux == "#"){
            qDebug() << data;
            break;
        }
        else{
            if(n == 0){
               data = aux;
            }
            else{
                 data += " " + aux;
            }
            n++;
        }
    }

    file.close();

    exit(0);

    qDebug() << FileIndex_Table::search_Primarykey(DIR+"/d3/cuenta/index", "dos");
    exit(0); */

    QDir dir(DIR);
    if( !dir.exists()){
        dir.mkdir(DIR);
        qDebug() << DIR << " : se creo este dir...";
    }
    else{
       qDebug() << DIR << " : dir creado...";
    }

    if(!QFile::exists(DIR_F_DATABASES)){
        FileDDL_DB::create_FDDL(DIR_F_DATABASES);
        qDebug() << DIR_F_DATABASES << " : se creo este fichero";
    }
    else{
        qDebug() << DIR_F_DATABASES << " : fichero creado...";
    }

    MainWindow w;
    w.show();
    
    return a.exec();
}
