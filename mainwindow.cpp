#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow){
    ui->setupUi(this);
    tconsole = new TextA_Console(ui->centralwidget);

    verticalLayout = new QVBoxLayout(ui->centralwidget);
    verticalLayout->setSpacing(6);
    verticalLayout->setContentsMargins(0, 0, 0, 0);

    verticalLayout->addWidget(tconsole);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete tconsole;
    delete verticalLayout;
}
