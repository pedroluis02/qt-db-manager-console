#include "texta_console.h"

TextA_Console::TextA_Console(QWidget *parent) :
    QTextEdit(parent){

    in_numbers = 0;

    QFont font;
    font.setPointSize(12);
    font.setWeight(50);
    setFont(font);

    setPalette(QPalette(QColor(204, 204, 204, 204)));

    list_types << "int" << "float" << "double" << "string" << "bool";

    set_in_Console_();
}

void TextA_Console::keyPressEvent(QKeyEvent *keyEvent){
    QTextEdit::keyPressEvent(keyEvent);
    switch(keyEvent->key()){
    case (Qt::Key_Enter - 1):
        QString input_all = document()->lastBlock().previous().text();
        int pos_pd = input_all.indexOf(":");
        QString input_text = input_all.right(input_all.size() - pos_pd - 1);
        list_tokens.clear();
        list_tokens = separateTokens(input_text);
        if(!list_tokens.empty()){
            parsing_tokens(list_tokens);
            in_numbers++;
        }

        set_in_Console_();

        int length = verticalScrollBar()->maximum() - verticalScrollBar()->minimum() +
                verticalScrollBar()->pageStep();
        verticalScrollBar()->setSliderPosition(length);

        break;
    }
}

QStringList TextA_Console::separateTokens(QString in){
    int i = 0;
    QString aux = ""; QStringList lks;
    bool error = false;
    while (i < in.size() && !error) {
        if(in.at(i) == ';' || in.at(i) == ','
                || in.at(i) == '\''
                || in.at(i) == '(' || in.at(i) == ')'){
            if(!aux.isEmpty()){
                lks << aux;
                aux = "";
            }
            lks << in.at(i);
        }
        else if(in.at(i).isSpace()){
            if(!aux.isEmpty()){
                lks << aux;
                aux = "";
            }
            i++;
            continue;
        }
        else{
            aux += in.at(i);
        }
        i++;
        if(i == in.size() && !aux.isEmpty()){
            lks << aux;
        }
    }
    return lks;
}

void TextA_Console::set_in_Console_(){
    textCursor().insertText("\n");
    textCursor().insertHtml("<font color=blue> In [ <b>" +
                              QString::number(in_numbers) +
                                           " </b>]:&nbsp;&nbsp;</font>&nbsp;");
}

void TextA_Console::set_out_list(QString title, QStringList dbs){
    QString title_format = "<b>&nbsp;&nbsp;"+title+"&nbsp;&nbsp;<b>";
    if(dbs.isEmpty()){
        textCursor().insertHtml(title_format);
        textCursor().insertText("\n");
        return;
    }
    QTextTable *tt = textCursor().insertTable(dbs.size() + 1, 1);
    textTables.append(tt);
    QTextTableFormat f;
    f.setBorder(0.0);tt->setFormat(f);
    tt->cellAt(0, 0).firstCursorPosition().insertHtml(title_format);
    int i = 1;
    foreach (QString db, dbs) {
        tt->cellAt(i, 0).firstCursorPosition().insertText("  "+db+"  ");
        i++;
    }
    textCursor().insertText("\n");
}

void TextA_Console::set_out_table_describe(QList<Reg_Colum> regs){
    QTextTable *tt = textCursor().insertTable(regs.size() + 1, 4);
    textTables.append(tt);
    QTextTableFormat f;
    f.setBorder(0.1);tt->setFormat(f);

    tt->cellAt(0, 0).firstCursorPosition().insertHtml("<b>&nbsp;&nbsp;Number&nbsp;&nbsp;<b>");
    tt->cellAt(0, 1).firstCursorPosition().insertHtml("<b>&nbsp;&nbsp;Name&nbsp;&nbsp;<b>");
    tt->cellAt(0, 2).firstCursorPosition().insertHtml("<b>&nbsp;&nbsp;Type&nbsp;&nbsp;<b>");
    tt->cellAt(0, 3).firstCursorPosition().insertHtml("<b>&nbsp;&nbsp;Primary key&nbsp;&nbsp;<b>");
    int i = 1;
    foreach (Reg_Colum reg_c, regs) {
        tt->cellAt(i, 0).firstCursorPosition().insertText("  "+QString::number(reg_c.reg_number)+"  ");
        tt->cellAt(i, 1).firstCursorPosition().insertText("  "+QString(reg_c.name)+"  ");
        tt->cellAt(i, 2).firstCursorPosition().insertText("  "+QString(reg_c.type)+"  ");
        tt->cellAt(i, 3).firstCursorPosition().insertText("  "+QString::number(reg_c.primary_key)+"  ");
        i++;
    }
    textCursor().insertText("\n");
}

void TextA_Console::set_out_table_select(QStringList columns_names,
                                         QList<QString *> &dataRow){
    QTextTable *tt = textCursor().insertTable(dataRow.size() + 1,
                                              columns_names.size());
    //qDebug() << columns_names.size();
    textTables.append(tt);
    QTextTableFormat f;
    f.setBorder(0.1);tt->setFormat(f);

    int k = 0;
    foreach (QString colum_name, columns_names) {
        tt->cellAt(0, k).firstCursorPosition().insertHtml("<b>&nbsp;&nbsp;"+
                                                          colum_name+"&nbsp;&nbsp;<b>");
        k++;
    }
   // if(sdr > 1){
        for (int i = 0; i < dataRow.size(); i++) {
            for (int j = 0; j < columns_names.size(); j++) {
                 tt->cellAt(i+1, j).firstCursorPosition().insertText("  "+(dataRow.value(i))[j]+"  ");
            }
        }
    //}

    textCursor().insertText("\n");
    textCursor().insertText("\n");
}

void TextA_Console::printError_in(QString token_stop){
    textCursor().insertHtml("<font color=red>Error en: "+
                            token_stop+"</font>");
    textCursor().insertText("\n");
}

void TextA_Console::printError_incomplete(){
    textCursor().insertHtml("<font color=red>Error: comando incompleto...</font>");
    textCursor().insertText("\n");
}

void TextA_Console::printMessage(QString message){
    textCursor().insertHtml("<font color=green> "+message+" </font>");
    textCursor().insertText("\n");
}

int TextA_Console::isIdentifier(QString token_text){
    int i = 0;
    if(!token_text.at(i).isLetter()){
        return i;
    }

    if(token_text.size() == 1)
        return -1;

    i++;

    while(i < token_text.size()){
        if(!token_text.at(i).isLetterOrNumber()
                && token_text.at(i) != '_')
            return i;
        i++;
    }

    return -1;
}

/*int TextA_Console::isCharacter(QString token_text)
{
    if(token_text.size() != 1){
        return 0;
    }
    return 1;
}*/

int TextA_Console::isString(QString token_text){
    return -1;
}

int TextA_Console::isInteger(QString token_text){
    int i = 0;
    if(token_text.at(i) == '+'
            || token_text.at(i) == '-')
        i++;
    while(i < token_text.size()){
        if(!token_text.at(i).isDigit())
            return i;
        i++;
    }
    return -1;
}

int TextA_Console::isReal(QString token_text, bool &decimal){
    int i = 0;
    if(token_text.at(i) == '+'
            || token_text.at(i) == '-')
        i++;
    bool pd = false; decimal = false;//decimal
    while(i < token_text.size()){
        if(token_text.at(i) == '.'){
            if(!pd){
                pd = true;
                decimal = true;
            }
            else{
                return i;
            }
        }
        else if(!token_text.at(i).isDigit()){
            return i;
        }

        i++;
    }
    return -1;
}

int TextA_Console::isNumber(QString token_text, bool &decimal){
    return isReal(token_text, decimal);
}

void TextA_Console::parsing_tokens(QStringList tks){
    QString first = tks.first().toLower();
    if(first == "quit"){//done
        if(tks.size() == 1){
            printError_incomplete();
        }
        else if(tks.value(1) == ";"){
            if(tks.size() == 2){
                int ret =
                        QMessageBox::
                        question(this, tr("Close GestorPL"),
                                tr("like really close to manager?"),
                                 QMessageBox::Cancel
                                 | QMessageBox::Ok,
                                 QMessageBox::Cancel);
                switch(ret){
                    case QMessageBox::Ok: exit(0); break;
                }
            }else{
                printError_in(tks.value(2));
            }
        }
    }
    else if(first == "clear"){
        if(tks.size() == 1){
            printError_incomplete();
        }
        else if(tks.value(1) == ";"){
            //
            clear();
        }
    }
    else if(first == "create"){// done
        analyze_create(tks);
    }
    else if(first == "drop"){// done
        analyze_drop(tks);
    }
    else if(first == "show"){ //done
        analyze_show(tks);
    }
    else if(first == "use"){ //done
        analyze_use(tks);
    }
    else if(first == "describe"){// done
        analyze_describe(tks);
    }
    else if(first == "insert"){
        analyze_insert(tks);
    }
    else if(first == "select"){
        analyze_select(tks);
    }
    else{
        printError_in(tks.value(0));
    }
}

void TextA_Console::analyze_create(QStringList tks){
    if(tks.size() == 1){
        printError_incomplete();
        return;
    }

    int count_create = 1;

    if(tks.value(count_create).toLower() == "database"){
        count_create++;
        if(count_create == tks.size()){
            printError_incomplete();
            return;
        }

        QString new_db_name = tks.value(count_create);

        if(isIdentifier(new_db_name) == -1){
            count_create++;
            if(tks.value(count_create) == ";"){
                if(count_create == (tks.size() - 1)){
                    Result_OP res = DDL_DataBasePL::createDB(new_db_name);
                    if(res.state == 1){
                        printMessage("Base de Datos '"+new_db_name+"' creada correctamente.");
                    }
                    else{
                        printError_in("'"+new_db_name+"' "+res.description);
                    }
                }
                else{
                    count_create++;
                    printError_in(tks.value(count_create));
                }
            }else{

                printError_incomplete();
            }

        }else{
            printError_in(new_db_name);
        }
    }
    else if(tks.value(count_create).toLower() == "table"){
        count_create++;
        if(count_create == tks.size()){
            printError_incomplete();

            return;
        }

        QString new_table_name = tks.value(count_create);

        if(isIdentifier(new_table_name) == -1){
            analyze_structTable(tks, new_table_name, count_create);
        }else{
            printError_in(new_table_name);
        }
    }
    else{
        printError_incomplete();
        printError_in(tks.value(count_create));
    }

}

void TextA_Console::analyze_structTable(QStringList tks, QString new_table, int position){
    int count_struct = position;
    count_struct++;

    if(tks.value(count_struct) == "("){
        count_struct++;
        if(count_struct == tks.size()){
            printError_incomplete();
            return;
        }

        QString dec = "";
        QStringList names, types;
        while(true){
            dec = tks.value(count_struct);
            if(isIdentifier(dec) == -1){
                names << dec;

                count_struct++;
                dec = tks.value(count_struct);
                if(list_types.contains(dec.toLower())){
                    types << dec;

                    count_struct++;

                    if(count_struct == tks.size()){
                        printError_incomplete();
                        return;
                    }

                    dec = tks.value(count_struct);
                    if(dec == ","){
                        count_struct++;

                        if(count_struct == tks.size()){
                            printError_in(dec);
                            return;
                        }

                        if(tks.value(count_struct).toLower() == "primary"){
                            dec = "primary";
                            break;
                        }

                    }
                    else{
                        printError_in(dec);
                        return;
                    }
                }
                else{
                    printError_in("'"+dec+"' no es un tipo de dato.");
                    return;
                }
            }
            else{
                printError_in(dec);
                return;
            }
        }

        count_struct++;
        if(count_struct == tks.size()){
            printError_incomplete();
            return;
        }
        dec = tks.value(count_struct).toLower();
        if(dec == "key"){
            count_struct++;

            if(count_struct == tks.size()){
                printError_incomplete();
                return;
            }

            QString primary_key = "";

            dec = tks.value(count_struct);
            if(isIdentifier(dec) != -1){
                printError_in(dec);
                return;
            }
            else{
                primary_key = dec;
                count_struct++;
                if(count_struct == tks.size()){
                    printError_incomplete();
                    return;
                }else{
                    dec = tks.value(count_struct);
                }
            }

            if(dec == ")"){
                count_struct++;

                if(count_struct == tks.size()){
                    printError_incomplete();
                    return;
                }

                if(tks.value(count_struct) == ";"){
                    if(count_struct == (tks.size() - 1)){
                        if(ddl_table.isDBSelected()){
                            Result_OP res = ddl_table.createTable(new_table, names, types, primary_key);
                            if(res.state == 1){
                                printMessage("Tabla '"+new_table+"' se creo correctamente.");
                            }else{
                                printError_in(res.description);
                            }
                        }else{
                            printError_in("No se ha seleccionado ninguna Base de Datos.");
                        }
                    }
                    else{
                        count_struct++;
                        printError_in(tks.value(count_struct));
                    }
                }
                else{
                    printError_in(tks.value(count_struct));
                }
            }
            else{
                printError_in(dec);
            }
        }
        else{
            printError_in(dec);
        }
    }
    else{
        printError_incomplete();
    }
}

void TextA_Console::analyze_describe(QStringList tks){
    int count_describe = 1;
    if(tks.size() == 1){
        printError_incomplete();
        return;
    }

    QString table_name = tks.value(count_describe);

    if(isIdentifier(table_name) == -1){
        count_describe++;
        if(tks.value(count_describe) == ";"){
            if(count_describe == (tks.size() - 1)){
                if(ddl_table.isDBSelected()){
                    QList <Reg_Colum> regs;
                    Result_OP res = ddl_table.describeTable(table_name, regs);
                    if(res.state == 1){
                        set_out_table_describe(regs);
                    }
                    else{
                        printError_in(res.description );
                    }
                }else{
                    printError_in("No se ha seleccionado ninguna Base de Datos.");
                }
            }
            else{
                count_describe++;
                printError_in(tks.value(count_describe));
            }
        }
        else{
            printError_incomplete();
        }
        return;
    }
    else{
        printError_in(table_name);
    }
}

void TextA_Console::analyze_show(QStringList tks){
    int count_show = 1;
    if(tks.size() == 1){
        printError_incomplete();
        return;
    }
    if(tks.value(count_show).toLower() == "databases"){
        count_show++;
        if(tks.value(count_show) == ";"){
            if(count_show == (tks.size() - 1)){
                set_out_list("Data Bases", DDL_DataBasePL::showDBs());
            }
            else{
                count_show++;
                printError_in(tks.value(count_show));
            }
        }
        else{
            printError_incomplete();
        }
    }
    else if(tks.value(count_show).toLower() == "tables"){
        count_show++;
        if(tks.value(count_show) == ";"){
            if(count_show == (tks.size() - 1)){
                if(ddl_table.isDBSelected())
                    set_out_list("Tables "+ddl_table.databaseName(), ddl_table.showTables());
                else
                    printError_in("No se ha seleccionado ninguna Base de Datos.  ");
            }
            else{
                count_show++;
                printError_in(tks.value(count_show));
            }
        }
        else{
            printError_incomplete();
        }
    }
    else{
        printError_incomplete();
        printError_in(tks.value(count_show));
    }
}

void TextA_Console::analyze_use(QStringList tks){
    if(tks.size() == 1){
        printError_incomplete();
        return;
    }

    int count_use = 1;

    QString database_name = tks.value(count_use);
    if(isIdentifier(database_name) == -1){
        count_use++;
        if(tks.value(count_use) == ";"){
            if(count_use == (tks.size() - 1)){
                Result_OP res = DDL_DataBasePL::useDB(database_name);
                if(res.state == 1){
                    ddl_table.setDataBaseName(database_name);
                    printMessage("Base de Datos a usar: " + database_name);
                }
                else{
                    printError_in(res.description);
                }
            }
            else{
                count_use++;
                printError_in(tks.value(count_use));
            }
        }
        else{
            printError_incomplete();
        }
    }
    else{
        printError_incomplete();
        printError_in(database_name);
    }
}

void TextA_Console::analyze_drop(QStringList tks){
    if(tks.size() == 1){
        printError_incomplete();
        return;
    }

    int count_drop = 1;

    if(tks.value(count_drop).toLower() == "database"){
        count_drop++;
        if(count_drop == tks.size()){
            printError_incomplete();
            return;
        }
        QString db_name = tks.value(count_drop);
        if(isIdentifier(db_name) == -1){
            count_drop++;
            if(tks.value(count_drop) == ";"){
                if(count_drop == (tks.size() - 1)){
                    if(db_name == ddl_table.databaseName()){
                        printError_in("Base de Datos '"+db_name+"' se esta usando.");
                    }else{
                        Result_OP res = DDL_DataBasePL::dropBD(db_name);
                        if(res.state == 1)
                            printMessage("Base de datos '"+db_name+"' eliminada.");
                        else
                            printError_in(res.description);
                    }
                }else{
                    count_drop++;
                    printError_in(tks.value(count_drop));
                }
            }else{
                printError_incomplete();
            }
        }else{
            printError_in(db_name);
        }
    }
    else if(tks.value(count_drop).toLower() == "table"){
        count_drop++;
        if(count_drop == tks.size()){
            printError_incomplete();
            return;
        }

        QString table_name = tks.value(count_drop);

        if(isIdentifier(table_name) == -1){
            count_drop++;
            if(tks.value(count_drop) == ";"){
                if(count_drop == (tks.size() - 1)){
                    if(ddl_table.isDBSelected()){
                        Result_OP res = ddl_table.dropTable(table_name);
                        if(res.state == 1)
                            printMessage("Tabla '"+table_name+"' eliminada.");
                        else
                            printError_in(res.description);
                    }else{
                        printError_in("No se ha seleccionado ninguna Base de Datos.");
                    }
                }
                else{
                    count_drop++;
                    printError_in(tks.value(count_drop));
                }
            }else{
                printError_incomplete();
            }
        }else{
            printError_in(table_name);
        }
    }
    else{
        printError_in(tks.value(count_drop));
    }
}

void TextA_Console::analyze_insert(QStringList tks){
    QString table_name, token; bool aux_type;
    QStringList params, types;
    int state = 1, i = 1;
    while(i < tks.size()){
        token = tks.value(i);
        if(state == 1 && token.toLower() == "into"){
            state = 2;
        }
        else if(state == 2 && isIdentifier(token) == -1){
            table_name = token;
            state = 3;
        }
        else if(state == 3 && token.toLower() == "values"){
            state = 4;
        }
        else if(state == 4 && token == "("){
            state = 5;
        }
        else if(state == 5 && isNumber(token, aux_type) == -1){
            params.append(token);
            if(aux_type)
                types.append("real");
            else
                types.append("int");

            state = 6;
        }
        else if(state == 5 && (token.toLower() == "true"||
                               token.toLower() == "false")){
            params.append(token);
            types.append("bool");
            state = 6;
        }
        else if(state == 5 && token == "'"){
            state = 9;
        }
        else if(state == 9 && isString(token) == -1){
            params.append(token);
            types.append("string");
            state = 10;
        }
        else if(state == 10 && token == "'"){
            state = 6;
        }
        else if(state == 6 && token == ","){
            state = 5;
        }
        else if(state == 6 && token == ")"){
            state = 7;
        }
        else if(state == 7 && token == ";"){
            state = 8;
        }
        else{
            printError_in(token);
            return;
        }
        i++;
    }

    if(state != 8){
        printError_in(tks.value(i-1));
        return;
    }
    else{
        if(!ddl_table.isDBSelected()){
            printError_in("No se ha seleccionado ninguna Base de Datos.");
            return;
        }
        //qDebug() << params;
        Result_OP res = ddl_table.insertDataTable(table_name, params, types);
        if(res.state == 0)
            printError_in(res.description);
        else
            printMessage("Ok datos insertados correctamente.");
    }
}

void TextA_Console::analyze_select(QStringList tks){
    int i = 1, state = 1;
    QString ts, table_name;
    QStringList columns_names;
    while(i < tks.size()){
        ts = tks.value(i);

        if(state == 1 && ts == "*"){
            columns_names.append("*");
            state = 2;
        }
        else if(state == 1 && isIdentifier(ts) == -1){
            columns_names.append(ts);
            state = 3;
        }
        else if(state == 2 && ts.toLower() == "from"){
            state = 5;
        }
        else if(state == 2 && ts == ","){
            state = 4;
        }
        else if(state == 3 && ts == ","){
            state = 4;
        }
        else if(state == 3 && ts.toLower() == "from"){
            state = 5;
        }
        else if(state == 4 && isIdentifier(ts) == -1){
            columns_names.append(ts);
            state = 3;
        }
        else if(state == 5 && isIdentifier(ts) == -1){
            table_name = ts;
            state = 6;
        }
        else if(state == 6 && ts == ";"){
            state = 7;
        }
        else{
            printError_in(ts); columns_names.clear(); return;
        }

        i++;
    }

    if(state != 7){
        printError_in(tks.value(i - 1));
    }
    else{
        if(!ddl_table.isDBSelected()){
            printError_in("No se ha seleccionado ninguna Base de Datos.");
            return;
        }
        QStringList columns_names_out; QList <QString*> dataRows;
        Result_OP res = ddl_table.selectDataTable(table_name, columns_names,
                                                     columns_names_out, dataRows);
        if(res.state == 0){
            printError_in(res.description);
            return;
        }
        //qDebug() << columns_names;
        set_out_table_select(columns_names_out, dataRows);
        dataRows.clear();
        printMessage("Consulta correcta.");
    }
}

TextA_Console::~TextA_Console(){
    for (int i = 0; i < textTables.size(); i++) {
        delete textTables.at(i);
    }
}
//
