#ifndef FILES_DDL_H
#define FILES_DDL_H

#include <QFileInfoList>
#include <QFileInfo>

#include <QIODevice>
#include <QFile>
#include <QDir>

#include <QStringList>
#include <QString>

#include <QDebug>

/* Cabecera del fichero*/
class Header_F{
public:
    int reg_numbers; // número de registros.
    int reg_emptys;  // registro vacio tras una eliminación.
};

/*Registro de datos*/
class Reg_Data{
public:
    int reg_number;
    char item_name[30];
    int state; // [0 | 1]
};

class FileDDL_DB
{
public:
    static bool create_FDDL(QString dir_file);
    static bool create_FDDL_Empty(QString dir_file);
    static bool write_FDDL(QString dir_file, Reg_Data reg_in);
    static Reg_Data read_FDDL(QString dir_file, int reg_number);
    static bool delete_FDDL(QString dir_file, int reg_number);
    static QStringList readAll_FDDL(QString dir_file);
    static int search_FDDL(QString dir_file, QString item_s);
    static int numRegs(QString dir_file);
};

class DirDDL_DB{
public:
    static bool removeDir_DB(QString dir_pro, QString db_name);
    static bool removeDir_Table(QString dir_db, QString table_name);
};

//---struct tables

class Reg_Colum{
public:
    int reg_number;
    char name[30];
    char type[30];
    bool primary_key;
};

class FileStruct_Table{
public:
    static bool write_AllColums(QString dir_file, QList<Reg_Colum> regs);
    static QList <Reg_Colum> read_AllColums(QString dir_file);
};

//----data table

class Reg_Index{
public:
    int  reg_number;
    char primary_key[50];
    long data_position;
    int left;
    int right;
};

class FileIndex_Table{
public:
    static bool write_RegIndex(QString dir_file, Reg_Index);
    static QList <long> readAllIndex(QString dir_file);
    static long search_Primarykey(QString dir_file, QString key);
};

class FileData_Table{
public:
    static long write_dataRow(QString dir_file, QStringList news_params);
    static QList <QString*> readAll_DataT(QString dir_file, QList <long> indexs,
                              QList<Reg_Colum> columns_out);
};

#endif // FILES_DDL_H
