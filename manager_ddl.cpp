#include "manager_ddl.h"

Result_OP DDL_DataBasePL::createDB(QString db_name){
    int s = FileDDL_DB::search_FDDL(DIR_F_DATABASES, db_name);
    Result_OP res;
    if(s != -1){
        res.state = 0;
        res.description = "Base de Datos '" + db_name +"' ya existe.";
    }
    else{
        res.state = 1;
        QDir dir(DIR);
        dir.mkdir(db_name);
        Reg_Data reg_data;
        strcpy(reg_data.item_name, db_name.toLatin1().data());
        reg_data.state = 1;
        FileDDL_DB::write_FDDL(DIR_F_DATABASES, reg_data);
        QString dir_f_t = DIR + "/" + db_name + "/" + TABLES_FILE;
        FileDDL_DB::create_FDDL(dir_f_t);
    }
    return res;
}

Result_OP DDL_DataBasePL::useDB(QString db_name){
    int s = FileDDL_DB::search_FDDL(DIR_F_DATABASES, db_name);
    Result_OP res;
    if(s == -1){
        res.state = 0;
        res.description = "Base de Datos '" + db_name +"' no existe.";
    }
    else{
        res.state = 1;
    }
    return res;
}

QStringList DDL_DataBasePL::showDBs(){
    return FileDDL_DB::readAll_FDDL(DIR_F_DATABASES);
}

Result_OP DDL_DataBasePL::dropBD(QString db_name){
    int s = FileDDL_DB::search_FDDL(DIR_F_DATABASES, db_name);
    Result_OP res;
    if(s == -1){
        res.state = 0;
        res.description = "Base de Datos '" + db_name +"' no existe.";
    }
    else{
        res.state = 1;
        DirDDL_DB::removeDir_DB(DIR, db_name);
        FileDDL_DB::delete_FDDL(DIR_F_DATABASES, s);
    }
    return res;
}

/*---------------------------------------------*/

DDL_TablesPL::DDL_TablesPL(QString db_name){
    this->db_name = db_name;
    if(!db_name.isEmpty()){
        dir_DBT = DIR + "/" + db_name;
    }
}

void DDL_TablesPL::setDataBaseName(QString db_name){
    this->db_name = db_name;
     dir_DBT = DIR + "/" + db_name;
}

QString DDL_TablesPL::databaseName() const{
    return db_name;
}

Result_OP DDL_TablesPL::createTable(QString table_name,
                                    QStringList names,
                                    QStringList types,
                                    QString primary_key){
    QString F_DB_TABLE = dir_DBT + "/" + TABLES_FILE;
    int s = FileDDL_DB::search_FDDL(F_DB_TABLE, table_name);
    Result_OP res;
    if(s != -1){
        res.state = 0;
        res.description = "Tabla '"+ table_name +"' ya existe.";
    }
    else{
        QList <Reg_Colum> regs = insertDefinitionTable(names, types, primary_key);
        if(regs.isEmpty()){
            res.state = 0;
            res.description = "clave primaria no existe.";
            return res;
        }

        res.state = 1;
        Reg_Data reg_data;
        strcpy(reg_data.item_name, table_name.toLatin1().data());
        reg_data.state = 1;
        QDir dir(dir_DBT);
        dir.mkdir(table_name);
        FileDDL_DB::write_FDDL(F_DB_TABLE, reg_data);
        FileDDL_DB::create_FDDL_Empty(dir_DBT + "/" + table_name + "/" + table_name);
        FileDDL_DB::create_FDDL_Empty(dir_DBT + "/" + table_name + "/" + STRUCT_FILE);
        FileDDL_DB::create_FDDL_Empty(dir_DBT + "/" + table_name + "/" + INDEX_FILE);
        FileStruct_Table::write_AllColums(dir_DBT + "/" + table_name + "/" + STRUCT_FILE,
                                          regs);
    }
    return res;
}

QStringList DDL_TablesPL::showTables(){
    QString DIR_DB_TABLE = dir_DBT + "/" + TABLES_FILE;
    return FileDDL_DB::readAll_FDDL(DIR_DB_TABLE);
}

Result_OP DDL_TablesPL::dropTable(QString table_name){
    QString DIR_DB_TABLE = dir_DBT + "/" + TABLES_FILE;
    int s = FileDDL_DB::search_FDDL(DIR_DB_TABLE, table_name);
    Result_OP res;
    if(s == -1){
        res.state = 0;
        res.description = "Tabla '"+ table_name +"' no existe.";
    }
    else{
        res.state = 1;
        DirDDL_DB::removeDir_Table(dir_DBT, table_name);
        FileDDL_DB::delete_FDDL(DIR_DB_TABLE, s);
    }
    return res;
}

Result_OP DDL_TablesPL::describeTable(QString table_name, QList<Reg_Colum> &regs){
    QString DIR_DB_TABLE = dir_DBT + "/" + TABLES_FILE;
    int s = FileDDL_DB::search_FDDL(DIR_DB_TABLE, table_name);
    Result_OP res;
    if(s == -1){
        res.state = 0;
        res.description = "Tabla '"+ table_name +"' no existe.";
        return res;
    }
    res.state = 1;
    QString DIR_T_S = dir_DBT + "/" + table_name + "/" + STRUCT_FILE;
    regs = FileStruct_Table::read_AllColums(DIR_T_S);
    return res;
}

///
Result_OP DDL_TablesPL::insertDataTable(QString table_name, QStringList params,
                                         QStringList types_input){
    QString DIR_DB_TABLE =  dir_DBT + "/" + TABLES_FILE;
    int s = FileDDL_DB::search_FDDL(DIR_DB_TABLE, table_name);
    Result_OP res; res.state = 0;
    if(s == -1){
        res.description = "Tabla '"+ table_name +"' no existe.";
        return res;
    }
   QString DIR_T_S = dir_DBT + "/" + table_name + "/" + STRUCT_FILE;
   QList<Reg_Colum> regs = FileStruct_Table::read_AllColums(DIR_T_S);
   if(regs.size() != params.size()){
       res.description = "Numero de parametros incorrecto.";
       regs.clear(); params.clear();
       return res;
   }
   int i = 0; QStringList news_params_ok;
   int primary_key = -1;
   foreach (QString v, params) {

       if(regs.at(i).primary_key == 1){
           primary_key = i;
       }

       //qDebug() << QString(regs.at(i).type);

       if(QString(regs.at(i).type) == "bool"){
           if(types_input.value(i) == "bool"){
               v = (v == "true") ? "1" : "0";
               news_params_ok.append(v);
           }
           else if(types_input.value(i) == "int" && v.size() == 1 &&
                   (v == "0" || v == "1")){
               news_params_ok.append(v);
           }else{
               res.description = QString(regs.at(i).name) + " requiere un solo BOOL.";
               return res;
           }
       }
       else if(QString(regs.at(i).type) == "string"){
           news_params_ok.append(v);
       }
       else if(QString(regs.at(i).type) == "int"){
           if(types_input.value(i) == "int"){
               news_params_ok.append(v);
           }
           else{
               res.description = QString(regs.at(i).name) + " requiere INT.";
               return res;
           }

       }
       else if(QString(regs.at(i).type) == "float"){
           if(types_input.value(i) == "real"){
               news_params_ok.append(v);
           }
           else{
               res.description = QString(regs.at(i).name) + " requiere FLOAT.";
               return res;
           }
       }
       else if(QString(regs.at(i).type) == "double"){
           if(types_input.value(i) == "real"){
               news_params_ok.append(v);
           }
           else{
               res.description = QString(regs.at(i).name) + " requiere DOUBLE.";
               return res;
           }
       }
       i++;
   }

   //qDebug() << news_params_ok;
   //return res;

   QString DIR_I_T = dir_DBT + "/" +
           table_name + "/" + INDEX_FILE;
   long s_pk = FileIndex_Table::search_Primarykey(DIR_I_T,
                                                  news_params_ok.at(primary_key));
   if(s_pk != -1){
       res.description = "Clave primaria duplicada.";
       return res;
   }

   // test de prueba 2
   // qDebug() << s_pk;
   // return res;

   long reg_pos = DML_Table::insertDataT(dir_DBT + "/" + table_name + "/" + table_name,
                          news_params_ok);

   Reg_Index reg_i;
   reg_i.data_position = reg_pos;
   strcpy(reg_i.primary_key, news_params_ok.at(primary_key).toLatin1().data());
   FileIndex_Table::write_RegIndex(DIR_I_T, reg_i);
   res.state = 1;
   return res;
}

Result_OP DDL_TablesPL::selectDataTable(QString table_name, QStringList columns_names_in,
                                        QStringList &columns_names_out, QList <QString*> &dataRows){
    QString DIR_DB_TABLE = dir_DBT + "/" + TABLES_FILE;
    int s = FileDDL_DB::search_FDDL(DIR_DB_TABLE, table_name);
    Result_OP res; res.state = 0;
    if(s == -1){
        res.description = "Tabla '"+ table_name +"' no existe.";
        return res;
    }
    QString DIR_I_T =  dir_DBT + "/" + table_name + "/" + INDEX_FILE;
    QString DIR_D_T =  dir_DBT + "/" + table_name + "/" + table_name;
    QString DIR_S_T =  dir_DBT + "/" + table_name + "/" + STRUCT_FILE;

    QList <Reg_Colum> dataColumns = FileStruct_Table::read_AllColums(DIR_S_T);

    QStringList aux_columns;
    QList <Reg_Colum> columns_out;

    foreach (Reg_Colum reg_c, dataColumns) {
        aux_columns.append(QString(reg_c.name));
    }

    s = -1;
    foreach (QString cni, columns_names_in) {
        if(cni == "*"){
            foreach (Reg_Colum reg_c, dataColumns) {
                columns_names_out.append(reg_c.name);
                columns_out.append(reg_c);
            }
            continue;
        }

        s = searchColumnIndex(aux_columns, cni);

        if(( s != -1)){
            //qDebug() << "s: " << s;
            columns_names_out.append(cni);
            columns_out.append(dataColumns.at(s));
        }
        else{
            res.description = "Columna '"+ cni +"' no existe.";
            return res;
        }
    }

    QList <long> indexs = FileIndex_Table::readAllIndex(DIR_I_T);
    if(!indexs.isEmpty()){
        dataRows = FileData_Table::readAll_DataT(DIR_D_T, indexs, columns_out);
    }
    res.state = 1;
    return res;
}

///
bool DDL_TablesPL::isDBSelected(){
    return !db_name.isEmpty();
}

int DDL_TablesPL::searchColumnIndex(QStringList columns_names,
                                    QString column_name){
    int i = 0;
    // qDebug() << columns_names;
    // qDebug() << column_name;
    foreach (QString cn, columns_names) {
        if(cn == column_name)
            return i;
        i++;
    }
    return -1;
}

QList<Reg_Colum> DDL_TablesPL::insertDefinitionTable(QStringList names,
                                                     QStringList types,
                                         QString primary_key){
    QList <Reg_Colum> regs;
    if(!names.contains(primary_key))
        return regs;

    int i = 0;
    while(i < names.size()){
        Reg_Colum reg_c;
        reg_c.reg_number = i;
        strcpy(reg_c.name, names.at(i).toLatin1().data());
        strcpy(reg_c.type, types.at(i).toLatin1().data());
        if(primary_key == names.at(i)){
            reg_c.primary_key = 1;
        }
        else{
            reg_c.primary_key = 0;
        }
        regs.append(reg_c);
        i++;
    }

    return regs;
}

////

void DDLStruct_Table::insertStructT(QString dir_table, QList<Reg_Colum> regs){
    FileStruct_Table::write_AllColums(dir_table, regs);
}


long DML_Table::insertDataT(QString dir_table, QStringList params_ok){
    return FileData_Table::write_dataRow(dir_table, params_ok);
}
