#ifndef MANAGER_DDL_H
#define MANAGER_DDL_H

#include "files_ddl.h"

/*Const*/
static QString DIR = QDir::currentPath() + QString("/dataPL");
static QString DBS_FILE = "databases";
static QString DIR_F_DATABASES = DIR + "/" + DBS_FILE;
static QString TABLES_FILE = "tables";
static QString STRUCT_FILE = "struct";
static QString INDEX_FILE  = "index";
/**/

class Result_OP{
public:
    int state;
    QString description;
};

class DDL_DataBasePL
{
public:
    static Result_OP createDB(QString db_name);
    static Result_OP useDB(QString db_name);
    static QStringList showDBs();
    static Result_OP dropBD(QString db_name);
};

class DDL_TablesPL{
public:
    DDL_TablesPL(QString db_n="");
    void setDataBaseName(QString db_n);
    QString databaseName() const;

    Result_OP createTable(QString table_name,
                          QStringList names,
                          QStringList types,
                          QString primary_key);
    QStringList showTables();
    Result_OP dropTable(QString table_name);
    Result_OP describeTable(QString table_name, QList<Reg_Colum> &regs);

    Result_OP insertDataTable(QString table_name, QStringList params,
                              QStringList types_input);
    Result_OP selectDataTable(QString table_name, QStringList columns_names_in,
                            QStringList &columns_names_out, QList <QString*> &dataRows);

    bool isDBSelected();

    int searchColumnIndex(QStringList columns_names, QString columna_name);

private:

    QList<Reg_Colum> insertDefinitionTable(QStringList names,
                               QStringList types,
                               QString primary_key);

    QString db_name;
    QString dir_DBT;
};

class DDLStruct_Table{
public:
    static void insertStructT(QString  dir_table, QList <Reg_Colum> regs);
};

class DML_Table{
public:
    static long insertDataT(QString dir_table, QStringList params_ok);
};

#endif // GESTOR_DDL_H
